# Oikos static

Demo de Oikos como sitio estático.

## Instalación

Hay que tener Python 3 y Nodejs/npm.

Instalar pelican

Instalar tailwind


## Hacer cambios al sitio web

Todos los textos e imágenes del sitio van bajo el directorio
[content](content). Para hacer cambios hay que ser miembro de este
repositorio.


## Apariencia del sitio

Cambiar la apariencia del sitio requiere del dominio de html,
[Tailwind](https://tailwindcss.com/docs/customizing-colors),
[Jinja](https://pypi.org/project/Jinja2/) y
[Pelican](https://docs.getpelican.com/en/latest/content.html).

El directorio **theme** tiene plantillas de Jinja/HTML en el
subdirectorio *templates*. Las hojas de estilo se hacen con
[tailwind](https://tailwindcss.com/docs/customizing-colors), de modo
que los estilos se declaran en las etiquetas mismas del html. La única
hoja de estilo establece una configuración base para elementos como
<a> o <ul>, de los que no se tiene control pues Pelican los genera
desde Markdown.


## Ayuda

Preguntas sobre el mantenimiento a esta página web dirigirlas a
<rgarcia@iecologia.unam.mx>

