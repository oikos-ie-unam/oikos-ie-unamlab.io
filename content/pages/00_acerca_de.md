---
title: Oikos=
---

Revista de divulgación del Intituto de Ecología.

Oikos= es una publicación cuatrimestral, editada por la Universidad
Nacional Autónoma de México, Ciudad Universitaria, Delegación
Coyoacán, C.P. 04510, Ciudad de México, a través de la Unidad de
Divulgación y Difusión del Instituto de Ecología, Ciudad
Universitaria, Circuito Exterior S/N, Delegación Coyoacán, C.P. 04510,
México, Tel. (55)5622-9002, correo electrónico: <cequihua@iecologia.unam.mx>, 

Editor responsable: Luis Enrique Eguiarte Fruns.

El contenido de los artículos es responsabilidad de los autores y no
refleja el punto de vista de los árbitros, del Editor o de la unam. Se
autoriza la reproducción de los artículos (no así de las imágenes) con
la condición de citar la fuente y se respeten los derechos de autor.


