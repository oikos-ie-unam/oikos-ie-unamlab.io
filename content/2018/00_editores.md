---
category: Año 2/No. 22/De los editores
title: Las ciencias de la sostenibilidad
date: 2010-10-03 10:20
modified: 2010-10-04 18:40
tags: sostenibilidad, Oikos=22
authors: Lakshmi Charli Joseph, Laura Espinosa Asuar, Clementina Equihua Z., Luis E. Eguiarte
summary: Editorial del número especial dedicado a Ciencias de la Sostenibilidad.
image: /media/2018/adaptacion_inundaciones.jpg
---

Este número de Oikos= surge con motivo del décimo ani-
versario de la creación del Laboratorio Nacional de Ciencias
de la Sostenibilidad (lancis), proyecto promovido por el
Instituto de Ecología de la Universidad Nacional Autóno-
ma de México en 2009 (Ver Oikos= Inauguración del La-
boratorio Nacional de Ciencias de la Sostenibilidad. lancis
fue diseñado para fomentar el tránsito hacia la sostenibili-
dad de nuestro país, y una de sus tareas más importantes es
vincular la academia, con los diversos actores sociales, con-
tribuyendo de esta forma a la articulación de la ciencia y la
toma de decisiones.

El número está conformado por artículos que mues-
tran diferentes estudios en Ciencias de la Sostenibilidad
que actualmente se realizan desde la unam, ilustrando la
diversidad de participantes involucrados en ellos, que van
desde alumnos de doctorado del Posgrado en Ciencias de
la Sostenibilidad de la unam, a investigadores jóvenes que
comienzan a explorar esta temática, hasta investigadores
consolidados en el área.

En gran medida gracias al entusiasmo de los jóvenes
estudiantes de dicho posgrado se escribieron un total de 15
colaboraciones, todas ellas disponibles en pdf y en línea.
Las Ciencias de la Sostenibilidad estudian las in-
teracciones entre los sistemas biofísicos y sociales —es de-
cir, el acoplamiento de los sistemas socio-ecológicos— y se
definen sobretodo por los problemas que abordan y por la
contribución a sus posibles soluciones. Los artículos ilustran
la complejidad de los problemas de sostenibilidad en Méxi-
co, como son la sobre-explotación de recursos naturales y el
impacto que tienen a la satisfacción de las necesidades de las
generaciones presentes y futuras, la creciente pobreza y des-
igualdad ante el cambio global, entre otros. En algunos de
ellos se discute cómo estos problemas también son perver-
sos, es decir, no existe una solución única, y a veces las so-
luciones planteadas pueden agravar los problemas; sin em-
bargo, señalan la necesidad de la investigación para poder
identificarlos en primer lugar, y luego explorar las posibles
soluciones. Los textos reflejan un elemento fundamental de
esta disciplina: la transdisciplina, un enfoque de trabajo que
involucra tanto a actores académicos como no académicos,
desde los inicios de un proyecto cuando se conceptualizan
los problemas a atender, hasta la construcción conjunta de
alternativas de solución.

Oikos= 22 inicia con un relato de Marisa Mazari-Hi-
riart, María José Solares y Amy M. Lerner, en el que hablan
de los retos y lecciones aprendidas en la creación e imple-
mentación del Posgrado en Ciencias de la Sostenibilidad,
proyecto liderado por el Instituto de Ecología, y continúa
con diversos casos de estudio que se relacionan con la Ciu-
dad de México: Luis Bojórquez y Hallie Eakin describen
un proyecto de investigación que explora la vulnerabilidad
de las megaciudades, particularmente de la Cd. de México,
ante el cambio global. En otro artículo Cristina Ayala ana-
liza la situación de las áreas verdes en la Ciudad de México.
Contamos además con dos manuscritos que hablan sobre
el paradigmático humedal urbano de Xochimilco: Patricia
Pérez Belmont y Beatriz Ruizpalacios investigan temas de
transiciones rurales-urbanas y de basura, respectivamente.
El tema del agua, tan importante para nuestra ciudad, se
aborda en dos escritos: Alma Rosa Huerta y Adrián Pedrozo
Acuña analizan su manejo, y en otro, de Yosune Miquela-
jauregui, Erika Luna, Rodrigo García y Fidel Serrano-Can-
dela, se habla sobre cómo es posible estudiar este problema.
También presentamos en este número 22 de Oikos=
casos de estudio en otras áreas de nuestro país, con temas
que van desde las aparentes disyuntivas entre conservación
de recursos naturales y la subsistencia humana, escrito por
Malena Oliva, como el reto que representa la ganadería para
la conservación de los ecosistemas y sus posibles soluciones,
escrito por Rocío Santos, Karina Boege, Juan Fornoni y Cé-
sar Dominguez.


Shiara González Padrón revisa el desafío del agua en
comunidades humanas, en particular las huicholas del No-
roeste de nuestro país, mientras que Abril Cid describe qué
son los mapas de vulnerabilidad y reflexiona sobre su utilidad
para analizar la vulnerabilidad de la costa mexicana. Emilio
Rodríguez por otra parte discute cuáles son los umbrales im-
portantes para la protección de ballenas de Baja California
Sur, y cómo se determinan. Por último, Verónica Elena So-
lares Rojas y Alonso Aguilar Ibarra escriben sobre un caso de
estudio en Chiapas en el que analizan el impacto de la altera-
ción del ciclo del agua sobre la seguridad alimentaria.
Uno de los aspectos relevantes de la sostenibilidad es
la necesidad de conceptualizar esta disciplina en desarrollo,
para lo cual contamos con dos artículos que están relaciona-
dos con estos aspectos. Alejandra Hernández Terán y Ana E.
Escalante nos hablan sobre el impacto de la domesticación
y la ingeniería genética en la agrodiversidad; y Jesús Mario
Siqueiros y L García, reflexionan sobre la relación entre el
poder y la sostenibilidad, así como sobre la conexión entre
lo social y lo ambiental.

Estamos seguros que nuestros lectores estarán de
acuerdo en que el esfuerzo de preparar este número ha va-
lido la pena, ya que esta colección de 15 textos, en su con-
junto logra reflejar un panorama completo y actual sobre las
Ciencias de la Sostenibilidad en México.
En nuestro próximo número recordaremos al recien-
temente fallecido José Negrete Martínez, un pionero de la
ecología teórica y hablaremos sobre cómo es que aves como
los colibríes logran vivir sólo de azúcar, entre otras sorpresas.
Esperamos que disfruten este número 22 de Oikos= tan-
to como nosotros.
