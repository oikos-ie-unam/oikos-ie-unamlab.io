---
category: Año 2/No. 22/De la dirección
title: Agenda Thunberg
date: 2010-10-03 10:20
modified: 2010-10-04 18:40
tags: sostenibilidad, Oikos=22, Greta Thunberg
authors: Constantino Macías
summary: Greta Thunberg nos ha indicado con una voz que, sin ser la primera, sí se cuenta entre las más lúcidas y efectivas, una agenda inescapable: las sociedades tenemos que cambiar nuestra conducta pero en particular los políticos y las grandes corporaciones tienen que escuchar a los científicos.
---




<div class="md:pl-32 md:py-4 p-2 bg-slate-50  font-sans">
This ongoing irresponsible behaviour will no doubt be remembered in history as one of the greatest failures of humankind. (Este comportamiento irresponsable actual será sin duda, recordado en la historia como uno de los más grandes fallos de la humanidad).
<p class="text-right">
Greta Thunberg
</p>
</div>




Eso manifestó la joven en su intervención ante el Parlamento Británico el pasado abril. Parecería extraordinario que el más
antiguo parlamento occidental le diera audiencia a una chica de 16 años, sobre todo porque Greta no es un genio artístico
o académico, o una atleta consumada. No se trata de una princesa inglesa, o de la heredera de alguna casa real extranjera.
Greta ni siquiera es británica. Es una estudiante sueca a la que el acceso a la información, junto con su evidente racionali-
dad —supongo que nutrida por un sistema educativo sensato— la ha llevado a percibir con claridad cuáles son los riesgos
que enfrenta el sistema climático planetario, y cuales son las consecuencias que su desajuste, provocado por los humanos,
habrá de tener muy pronto.

Greta ha comprendido el llamado de los científicos y ha escuchado la angustia en sus voces; angustia nacida de la
certeza de una catástrofe inminente si no tomamos acciones radicales. También ha entendido por qué no las tomamos. En
esa misma intervención en Westminster, les dijo a los miembros del parlamento “ustedes no escuchan a la ciencia, porque
están solamente interesados en soluciones que les permitan seguir en lo suyo como antes”.
Reconocer cuáles son los procesos que nos han puesto en esta ruta de colisión, imaginar acciones efectivas e imple-
mentarlas con el concurso de los diferentes actores sociales para corregir el rumbo, es una buena definición de sostenibili-
dad. Greta Thunberg nos ha indicado con una voz que, sin ser la primera, sí se cuenta entre las más lúcidas y efectivas, una
agenda inescapable: las sociedades tenemos que cambiar nuestra conducta pero en particular los políticos y las grandes
corporaciones tienen que escuchar a los científicos. No podemos seguir pensando en proyectos de ningún tipo sin cuestio-
narnos cual será su impacto en el ambiente.


Se trata de una agenda de relevancia global, pero que contiene unas cuantas notas que resuenan con particular
agudeza en el México de hoy. En él se ha asomado la tentación de darle la espalda a la ciencia en favor de quehaceres corto-
placistas. Ello nos pone, como especie, en el riesgo ser más un obstáculo que un promotor de la urgente agenda ambiental.
Este número de Oikos= nos invita y nos informa sobre diversas manera de sumarnos a la agenda Thunberg.


