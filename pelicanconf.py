#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'IE'
SITENAME = 'Oikos='
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Mexico_City'

LOCALE = 'es_MX.utf-8'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

RELATIVE_URLS = False

ARTICLE_URL = '{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

THEME = 'theme'

STATIC_PATHS = ['media', ]

MENUITEMS = [
    ("Números anteriores", "/categories/"),
]
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
