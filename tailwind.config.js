module.exports = {
    content: ['./theme/templates/*.html',
	      './content/**/*.md',
	     ],
    theme: {
	extend: {
	    colors: {
		teal: {
		    850: '#165c61',
		},
		amber: {
		    450: '#e89615',
		},
	    },
	},
	plugins: [],
    }
}
